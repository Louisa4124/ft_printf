/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   testprintf.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: marvin <marvin@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/11/21 17:51:18 by marvin            #+#    #+#             */
/*   Updated: 2022/11/21 17:51:18 by marvin           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>

void ft_putnbr_base(unsigned long nbr)
{
	int	tmp;
	char *base;

	tmp = 0;
	base = "0123456789abcdef";

	if (nbr < 0)
	{
		write(1, "-", 1);
		nbr = nbr - (2 * nbr);
	}
	while (base[tmp] != '\0')
		tmp++;
	if (nbr < tmp)
		write(1, &base[nbr], 1);
	if (nbr >= tmp)
	{
		ft_putnbr_base(nbr / tmp);
		ft_putnbr_base(nbr % tmp);
	}
}

int main(void)
{
	int c = -1234;
	printf("%x \n", c);
	ft_putnbr_base(c);
	write(1, "\n", 1);
	return (0);
}
