# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: lboudjem <lboudjem@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2022/11/21 17:21:38 by lboudjem          #+#    #+#              #
#    Updated: 2022/11/21 17:21:38 by lboudjem         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #


NAME = libftprintf.a 

FLAGS = -Wall -Wextra -Werror -I .

SRCS =	ft_printf.c \
		ft_putchar.c \
		ft_puthexa_lowcase.c \
		ft_puthexa_upcase.c \
		ft_putnumber.c \
		ft_putpercent.c \
		ft_putpointer.c \
		ft_putstring.c \

SRCS_T	=	main.c

OBJS = ${SRCS:.c=.o}

OBJS_T = ${SRCS_T:.c=.o}

HEADER = ft_printf.h, libft.h

all :	${NAME}

${NAME}:	${OBJS}
		${AR} rcs $@ $^

test : 	${OBJS} ${OBJS_T}
		${CC} ${FLAGS} $^ && ./a.out && ${RM} a.out

%.o:	%.c ${HEADER} Makefile
		${CC} ${FLAGS} -c $< -o $@

clean:	
		${RM} ${OBJS}

fclean:	clean
		${RM} ${NAME}

re:	fclean
	${MAKE} all 

.PHONY: all clean fclean re