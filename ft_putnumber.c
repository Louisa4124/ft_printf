/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnumber.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: marvin <marvin@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/11/22 14:48:27 by marvin            #+#    #+#             */
/*   Updated: 2022/11/22 14:48:27 by marvin           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"
#include <stdio.h>

int	ft_putnbr(int nb)
{
	int	tmp;
	int	len;

	len = 0;
	if (nb == -2147483648)
		return (write(1, "-2147483648", 11));
	else
	{
		if (nb < 0)
		{
			nb = -nb;
			len += write(1, "-", 1);
		}
		if (nb > 9)
			len += ft_putnbr(nb / 10);
		tmp = nb % 10;
		tmp = tmp + 48;
		len += write(1, &tmp, 1);
	}
	return (len);
}

int	ft_putnbr_unsigned(unsigned int nb)
{
	unsigned int	tmp;
	int				len;

	len = 0;
	if (nb > 9)
		len += ft_putnbr_unsigned(nb / 10);
	tmp = nb % 10;
	tmp = tmp + 48;
	len += write(1, &tmp, 1);
	return (len);
}
