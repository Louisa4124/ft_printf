/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putpointer.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: marvin <marvin@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/11/22 11:05:30 by marvin            #+#    #+#             */
/*   Updated: 2022/11/22 11:05:30 by marvin           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int ft_putptr_base(unsigned long long p)
{
	unsigned long long	val;
	char *base;
	int	len;

	len = 0;
	val = (unsigned long long)p;
	base = "0123456789abcdef";
	if (val < 16)
		len += write(1, &base[val], 1);
	if (val >= 16)
	{
		len += ft_putptr_base(val / 16);
		len += ft_putptr_base(val % 16);
	}
	return (len);
}

int ft_putpointer(unsigned long long p)
{
	int len;

	len = 0;
	len += write(1, "0x", 2);
	len += ft_putptr_base(p);
	return (len);
}
