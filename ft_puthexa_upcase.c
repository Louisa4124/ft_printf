/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_puthexa_upcase.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: marvin <marvin@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/11/22 19:41:56 by marvin            #+#    #+#             */
/*   Updated: 2022/11/22 19:41:56 by marvin           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int ft_puthexa_upcase(unsigned long nbr)
{
	int	tmp;
	char *base;
	int	i;

	tmp = 0;
	i = 0;
	base = "0123456789ABCDEF";
	if (nbr < 0)
	{
		i += write(1, "-", 1);
		nbr = nbr - (2 * nbr);
	}
	while (base[tmp] != '\0')
		tmp++;
	if (nbr < tmp)
		i += write(1, &base[nbr], 1);
	if (nbr >= tmp)
	{
		i += ft_puthexa_upcase(nbr / tmp);
		i += ft_puthexa_upcase(nbr % tmp);
	}
	return (i);
}
