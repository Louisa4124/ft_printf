/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_puthexa_lowcase.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: marvin <marvin@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/11/22 19:31:10 by marvin            #+#    #+#             */
/*   Updated: 2022/11/22 19:31:10 by marvin           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int ft_puthexa_lowcase(unsigned long nbr)
{
	int	tmp;
	char *base;
	int	i;

	tmp = 0;
	i = 0;
	base = "0123456789abcdef";
	if (nbr < 0)
	{
		i += write(1, "-", 1);
		nbr = nbr - (2 * nbr);
	}
	while (base[tmp] != '\0')
		tmp++;
	if (nbr < tmp)
		i += write(1, &base[nbr], 1);
	if (nbr >= tmp)
	{
		i += ft_puthexa_lowcase(nbr / tmp);
		i += ft_puthexa_lowcase(nbr % tmp);
	}
	return (i);
}