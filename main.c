#include "ft_printf.h"
#include <stdio.h>
#include <limits.h>

int main(void)
{
	int tt = LONG_MIN;
	printf("p : %d\n", printf("c: %c &&&& ", 'A'));
	printf("p : %d\n", ft_printf("c: %c &&&& ", 'A'));
	printf("p : %d\n", printf("c: %s &&&& ", NULL));
	printf("p : %d\n", ft_printf("c: %s &&&& ", NULL));
	printf("p : %d\n", printf("c: %p &&&& ", tt));
	printf("p : %d\n", ft_printf("c: %p &&&& ", tt));
	printf("p : %d\n", printf("c: %d &&&& ", -2147483648));
	printf("p : %d\n", ft_printf("c: %d &&&& ", -2147483648));
	printf("p : %d\n", printf("c: %i &&&& ", 45));
	printf("p : %d\n", ft_printf("c: %i &&&& ", 45));
	printf("p : %d\n", printf("c: %u &&&& ", -100));
	printf("p : %d\n", ft_printf("c: %u &&&& ", -100));
	printf("p : %d\n", printf("c: %x &&&& ", 4294967295));
	printf("p : %d\n", ft_printf("c: %x &&&& ", 4294967295));
	printf("p : %d\n", printf("c: %X &&&& ", 4294967295));
	printf("p : %d\n", ft_printf("c: %X &&&& ", 4294967295));
	printf("p : %d\n", printf("c: %% &&&& "));
	printf("p : %d\n", ft_printf("c: %% &&&& "));
	printf("p : %d\n", printf("c: %c et %d et %s et %x", 'A', 58, "coucou", 15));
	printf("p : %d\n", printf("c: %c et %d et %s et %x", 'A', 58, "coucou", 15));
	printf("p : %d\n", printf(""));
	printf("p : %d\n", ft_printf(""));
	return (0);
}
