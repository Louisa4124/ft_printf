/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: marvin <marvin@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/11/21 17:25:56 by marvin            #+#    #+#             */
/*   Updated: 2022/11/21 17:25:56 by marvin           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_PRINTF_H
# define FT_PRINTF_H
# include <unistd.h>
# include <stdlib.h>
# include <stdarg.h>

int		ft_printf(const char *s, ...);
int		ft_putchar(int c);
int		ft_putstring(char *str);
int	 	ft_putpercent(void);
int		ft_puthexa_upcase(unsigned long nbr);
int		ft_puthexa_lowcase(unsigned long nbr);
int		ft_putnbr_unsigned(unsigned int nb);
int 	ft_putpointer(unsigned long long p);
int		ft_putnbr(int nb);
int		ft_putnbr_unsigned(unsigned int nb);

#endif